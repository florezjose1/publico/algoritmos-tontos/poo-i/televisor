### El Televisor

1. El televisor permite cambiar la entrada a tres tipos:
    * 0 = Apagado
    * 1 = Antena
    * 2 = Cable
    * 3 = Auxiliar (DVD, USB, HDMI, etc.)
2. El televisor permite ver el tipo de entrada, el canal y el volumen.
3. La cantidad de canales cuando la entrada es antena es sólo 13 y la cantidad de canales cuando la entrada es cable es 100. Cuando la entrada es auxiliar sólo se pueden usar los canales 1, 2 y 3. Por ahora no interesa guardar detalles de los canales, sólo controlar su cantidad.
4. El televisor permite cambiar de canal de manera secuencial hacia atrás y hacia adelante, dando vuelva desde el primer canal al último y viceversa.
5. El televisor muestra un indicador de error cuando se quiere subir volumen a más del máximo o menos del mínimo.


Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
